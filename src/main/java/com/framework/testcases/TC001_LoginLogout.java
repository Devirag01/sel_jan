package com.framework.testcases;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.framework.design.ProjectMethods;
import com.framework.pages.LoginPage;

public class TC001_LoginLogout extends ProjectMethods{

	@BeforeTest
	public void setData() {
		testCaseName = "TC001_LoginLogout";
		testDescription = "Login into leaftaps";
		testNodes = "Leads";
		author = "Devi";
		category = "smoke";
		dataSheetName = "TC001";
	}
	
	@Test(dataProvider="fetchData")
	public void login(String uname, String pwd) {
		new LoginPage()
		.enterUsername(uname) 
		.enterPassword(pwd)
		.clickLogin()
		.clickCrmsfa()
		.clickLeads()
		.clickCreateLead()
		.enterCompanyName()
		.enterFirstName()
		.enterLastName()
		.clickCreateLeadButton();
	}
	
}









