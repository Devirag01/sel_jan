package com.framework.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import com.framework.design.ProjectMethods;

public class CreateLeadPage extends ProjectMethods{

	public CreateLeadPage() {
       PageFactory.initElements(driver, this);
	}

	@FindBy(how = How.ID,using="createLeadForm_companyName") WebElement eleCname;
	public CreateLeadPage enterCompanyName() {
		clearAndType(eleCname, "Tl");
		return this; 
	}
	@FindBy(how = How.ID,using="createLeadForm_firstName") WebElement eleFname;
	public CreateLeadPage enterFirstName() {
		clearAndType(eleFname, "vidya");
		return this; 
	}
	@FindBy(how = How.ID,using="createLeadForm_lastName") WebElement eleLname;
	public CreateLeadPage enterLastName() {
		clearAndType(eleLname, "K");
		return this; 
	}
	@FindBy(how = How.NAME,using="submitButton") WebElement eleCLeadButton;
	public ViewLeadPage clickCreateLeadButton() {
		click(eleCLeadButton);
		return new ViewLeadPage();  
	}


}
















